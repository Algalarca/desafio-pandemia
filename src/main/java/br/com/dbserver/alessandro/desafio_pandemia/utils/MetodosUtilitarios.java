package br.com.dbserver.alessandro.desafio_pandemia.utils;

import java.time.Duration;
import java.time.LocalTime;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.dbserver.alessandro.desafio_pandemia.dto.RequisicaoNovoAtendimento;
import br.com.dbserver.alessandro.desafio_pandemia.model.Paciente;
import br.com.dbserver.alessandro.desafio_pandemia.model.User;
import br.com.dbserver.alessandro.desafio_pandemia.repository.PacienteRepository;
import br.com.dbserver.alessandro.desafio_pandemia.repository.UserRepository;

public class MetodosUtilitarios {
	
	@Autowired
	private PacienteRepository pacienteRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	public void salvaNovoAtendimento(RequisicaoNovoAtendimento requisicao, String username) {
		
		User usuario = userRepository.findByUsername(username);
		Paciente paciente = requisicao.toPaciente();
		paciente.setUser(usuario);
		
		pacienteRepository.save(paciente);
	}
	
	public static String calculaTempoDeAtendimento(LocalTime horaFimdoAtendimento, LocalTime horaInicioDoAtendimento) {
		
		Long horaEmMinutos = Duration.between(horaInicioDoAtendimento, horaFimdoAtendimento).toMinutes();
		Duration minutos = Duration.ofMinutes(horaEmMinutos);
		String tempoDeAtendimentoFormatado = LocalTime.MIN.plus(minutos).toString();
			
		return tempoDeAtendimentoFormatado;
	}
}
