package br.com.dbserver.alessandro.desafio_pandemia.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.dbserver.alessandro.desafio_pandemia.model.UnidadeDeAtendimento;

@Repository
public interface UnidadeDeAtendimentoRepository extends JpaRepository<UnidadeDeAtendimento, Integer> {

	UnidadeDeAtendimento findByNomeUnidadeDeAtendimento(String unidadeDeAtendimento);
}
