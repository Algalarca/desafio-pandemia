package br.com.dbserver.alessandro.desafio_pandemia.mvc.controller;

import java.security.Principal;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import br.com.dbserver.alessandro.desafio_pandemia.dto.RequisicaoNovoAtendimento;
import br.com.dbserver.alessandro.desafio_pandemia.model.Paciente;
import br.com.dbserver.alessandro.desafio_pandemia.model.User;
import br.com.dbserver.alessandro.desafio_pandemia.repository.PacienteRepository;
import br.com.dbserver.alessandro.desafio_pandemia.repository.UserRepository;

@Controller
@RequestMapping("mvc")
public class AtendimentoMVCController {
	
	@Autowired
	private PacienteRepository pacienteRepository;

	@Autowired
	private UserRepository userRepository;
	
	@GetMapping("listaAtendimentos") //o nome do método não precisa ser igual ao mapping
	public ModelAndView listaDeAtendimentos(Principal principal) {
		
		Sort sort = Sort.by("dataDoAtendimento").descending();
		PageRequest paginacao = PageRequest.of(0,  100, sort);
		
		Page<Paciente> pacientesPorDataDeAtendimento = pacienteRepository.findAll(paginacao);
		
		List<Paciente> pacientesPorAtendente = pacienteRepository.findAllByUsuario(principal.getName());
		
		ModelAndView mv = new ModelAndView("mvc/atendimento/listaAtendimentos");
		mv.addObject("pacientes", pacientesPorDataDeAtendimento);
		
		return mv;
	}
	
	public ModelAndView atendimentosRest() {
		
		Sort sort = Sort.by("dataDoAtendimento").descending();
		PageRequest paginacao = PageRequest.of(0,  100, sort);
		
		Page<Paciente> pacientesPorDataDeAtendimento = pacienteRepository.findAll(paginacao);
		
		ModelAndView mv = new ModelAndView("mvc/atendimento/formularioNovoAtendimento");
		mv.addObject("pacientesPorDataDeAtendimento", pacientesPorDataDeAtendimento);
		
		return mv;
	}
	
	@GetMapping("novoAtendimento")
	public String formularioNovoAtendimento(RequisicaoNovoAtendimento requisicao) {
		
		return "mvc/atendimento/formularioNovoAtendimento";
	}
	
	@PostMapping("novoAtendimento")
	public String novoAtendimento(@Valid RequisicaoNovoAtendimento requisicao, BindingResult result) {
		
		if(result.hasErrors()) {
			return "mvc/atendimento/formularioNovoAtendimento";
		}
		
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		User user = userRepository.findByUsername(username);
		
		Paciente paciente = requisicao.toPaciente();
		paciente.setUser(user);
		
		pacienteRepository.save(paciente);
		
		return "redirect:/mvc/novoAtendimento";
	}
	
	@ExceptionHandler(IllegalArgumentException.class)
	public String onError() {
		return "redirect:/mvc/listaAtendimentos";
	}

}
