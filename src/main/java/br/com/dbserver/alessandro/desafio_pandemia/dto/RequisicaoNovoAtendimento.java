package br.com.dbserver.alessandro.desafio_pandemia.dto;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import br.com.dbserver.alessandro.desafio_pandemia.model.Paciente;
import br.com.dbserver.alessandro.desafio_pandemia.model.resultadoDoTeste;
import br.com.dbserver.alessandro.desafio_pandemia.utils.MetodosUtilitarios;

public class RequisicaoNovoAtendimento {

	private static final DateTimeFormatter formataData = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	private static final DateTimeFormatter formataHora = DateTimeFormatter.ofPattern("H:mm");

	@NotBlank
	private String unidadeDeAtendimento;
	
	@NotBlank
	private String nomePaciente;
	
	@Pattern(regexp = "^\\d{11}$")
	@NotBlank
	private String cpf;
	
	private boolean suspeitaInfeccao;

	@Enumerated(EnumType.STRING)
	private resultadoDoTeste resultadoDoTeste01;
	
	@Enumerated(EnumType.STRING)
	private resultadoDoTeste resultadoDoTeste02;
	
	@Pattern(regexp = "^\\d{4}-\\d{2}-\\d{2}$")
	@NotBlank
	private String dataDoAtendimento;
	
	@NotBlank
	private String horaInicioDoAtendimento;
	
	@NotBlank
	private String horaFimDoAtendimento;
	
	private String tempoDeAtendimento;

	public String getUnidadeDeAtendimento() {
		return unidadeDeAtendimento;
	}
	public void setUnidadeDeAtendimento(String unidadeDeAtendimento) {
		this.unidadeDeAtendimento = unidadeDeAtendimento;
	}
	
	public String getNomePaciente() {
		return nomePaciente;
	}
	public void setNomePaciente(String nomePaciente) {
		this.nomePaciente = nomePaciente;
	}

	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public boolean isSuspeitaInfeccao() {
		return suspeitaInfeccao;
	}
	public void setSuspeitaInfeccao(boolean suspeitaInfeccao) {
		this.suspeitaInfeccao = suspeitaInfeccao;
	}

	public resultadoDoTeste getResultadoDoTeste01() {
		return resultadoDoTeste01;
	}

	public void setResultadoDoTeste01(resultadoDoTeste resultadoDoTeste01) {
		this.resultadoDoTeste01 = resultadoDoTeste01;
	}

	public resultadoDoTeste getResultadoDoTeste02() {
		return resultadoDoTeste02;
	}
	public void setResultadoDoTeste02(resultadoDoTeste resultadoDoTeste02) {
		this.resultadoDoTeste02 = resultadoDoTeste02;
	}

	public String getDataDoAtendimento() {
		return dataDoAtendimento;
	}
	public void setDataDoAtendimento(String dataDoAtendimento) {
		this.dataDoAtendimento = dataDoAtendimento;
	}

	public String getHoraInicioDoAtendimento() {
		return horaInicioDoAtendimento;
	}
	public void setHoraInicioDoAtendimento(String horaInicioDoAtendimento) {
		this.horaInicioDoAtendimento = horaInicioDoAtendimento;
	}

	public String getHoraFimDoAtendimento() {
		return horaFimDoAtendimento;
	}
	public void setHoraFimDoAtendimento(String horaFimDoAtendimento) {
		this.horaFimDoAtendimento = horaFimDoAtendimento;
	}

	public String getTempoDeAtendimento() {
		return tempoDeAtendimento;
	}
	public void setTempoDeAtendimento(String tempoDeAtendimento) {
		this.tempoDeAtendimento = tempoDeAtendimento;
	}

	public Paciente toPaciente() {
		Paciente paciente = new Paciente();
		String tempoDeAtendimentoCalculado;
		
		paciente.setNomePaciente(nomePaciente);
		paciente.setCpf(cpf);
		paciente.setSuspeitaInfeccao(suspeitaInfeccao);
		paciente.setResultadoDoTeste01(resultadoDoTeste01);
		paciente.setResultadoDoTeste02(resultadoDoTeste02);
		paciente.setDataDoAtendimento(LocalDate.parse(this.dataDoAtendimento, formataData));
		paciente.setHoraInicioDoAtendimento(LocalTime.parse(this.horaInicioDoAtendimento, formataHora));
		paciente.setHoraFimDoAtendimento(LocalTime.parse(this.horaFimDoAtendimento, formataHora));

		tempoDeAtendimentoCalculado = MetodosUtilitarios.calculaTempoDeAtendimento(paciente.getHoraFimDoAtendimento(), paciente.getHoraInicioDoAtendimento());
		paciente.setTempoDeAtendimento(LocalTime.parse(tempoDeAtendimentoCalculado, formataHora));
		
		return paciente;
	}
	
}
