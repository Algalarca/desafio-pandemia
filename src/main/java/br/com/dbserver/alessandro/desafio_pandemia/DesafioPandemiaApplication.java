package br.com.dbserver.alessandro.desafio_pandemia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@EnableCaching
@SpringBootApplication
public class DesafioPandemiaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesafioPandemiaApplication.class, args);
	}

}
