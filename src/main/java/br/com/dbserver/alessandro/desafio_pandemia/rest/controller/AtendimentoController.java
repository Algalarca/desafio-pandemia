package br.com.dbserver.alessandro.desafio_pandemia.rest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("rest")
public class AtendimentoController {

	@GetMapping()
	public String getListaPacientesRest() {
		
		return "rest/vuejs/atendimento/listaAtendimentos";
	}
	
	@GetMapping("novoAtendimento")
	public ModelAndView atendimentosRest() {
		
		ModelAndView mv = new ModelAndView("rest/vuejs/atendimento/novoAtendimento");
		
		return mv;
	}
	
	@ExceptionHandler(IllegalArgumentException.class)
	public String onError() {
		return "redirect:/rest/listaAtendimentos";
	}
}
