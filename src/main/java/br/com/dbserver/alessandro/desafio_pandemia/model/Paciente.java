package br.com.dbserver.alessandro.desafio_pandemia.model;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Paciente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String nomePaciente;
	private String cpf;
	private boolean suspeitaInfeccao;

	@Enumerated(EnumType.STRING)
	private resultadoDoTeste resultadoDoTeste01;
	
	@Enumerated(EnumType.STRING)
	private resultadoDoTeste resultadoDoTeste02;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	private User user;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore
	private UnidadeDeAtendimento unidadeDeAtendimento;
	
	private LocalDate dataDoAtendimento;
	private LocalTime horaInicioDoAtendimento;
	private LocalTime horaFimDoAtendimento;
	private LocalTime tempoDeAtendimento;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getNomePaciente() {
		return nomePaciente;
	}
	public void setNomePaciente(String nomePaciente) {
		this.nomePaciente = nomePaciente;
	}
	
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	public boolean isSuspeitaInfeccao() {
		return suspeitaInfeccao;
	}
	public void setSuspeitaInfeccao(boolean suspeitaInfeccao) {
		this.suspeitaInfeccao = suspeitaInfeccao;
	}
	
	public resultadoDoTeste getResultadoDoTeste01() {
		return resultadoDoTeste01;
	}
	public void setResultadoDoTeste01(resultadoDoTeste resultadoDoTeste01) {
		this.resultadoDoTeste01 = resultadoDoTeste01;
	}
	
	public resultadoDoTeste getResultadoDoTeste02() {
		return resultadoDoTeste02;
	}
	public void setResultadoDoTeste02(resultadoDoTeste resultadoDoTeste02) {
		this.resultadoDoTeste02 = resultadoDoTeste02;
	}
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	public UnidadeDeAtendimento getUnidadeDeAtendimento() {
		return unidadeDeAtendimento;
	}
	public void setUnidadeDeAtendimento(UnidadeDeAtendimento unidadeDeAtendimento) {
		this.unidadeDeAtendimento = unidadeDeAtendimento;
	}
	
	public LocalDate getDataDoAtendimento() {
		return dataDoAtendimento;
	}
	public void setDataDoAtendimento(LocalDate dataDoAtendimento) {
		this.dataDoAtendimento = dataDoAtendimento;
	}
	
	public LocalTime getHoraInicioDoAtendimento() {
		return horaInicioDoAtendimento;
	}
	public void setHoraInicioDoAtendimento(LocalTime horaInicioDoAtendimento) {
		this.horaInicioDoAtendimento = horaInicioDoAtendimento;
	}
	
	public LocalTime getHoraFimDoAtendimento() {
		return horaFimDoAtendimento;
	}
	public void setHoraFimDoAtendimento(LocalTime horaFimDoAtendimento) {
		this.horaFimDoAtendimento = horaFimDoAtendimento;
	}
	
	public LocalTime getTempoDeAtendimento() {
		return tempoDeAtendimento;
	}
	public void setTempoDeAtendimento(LocalTime tempoDeAtendimento) {
		this.tempoDeAtendimento = tempoDeAtendimento;
	}
	
}
