package br.com.dbserver.alessandro.desafio_pandemia.rest.api;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.dbserver.alessandro.desafio_pandemia.dto.RequisicaoNovoAtendimento;
import br.com.dbserver.alessandro.desafio_pandemia.model.Paciente;
import br.com.dbserver.alessandro.desafio_pandemia.model.UnidadeDeAtendimento;
import br.com.dbserver.alessandro.desafio_pandemia.model.User;
import br.com.dbserver.alessandro.desafio_pandemia.repository.PacienteRepository;
import br.com.dbserver.alessandro.desafio_pandemia.repository.UnidadeDeAtendimentoRepository;
import br.com.dbserver.alessandro.desafio_pandemia.repository.UserRepository;

@RestController
@RequestMapping("api")
public class PacientesRest {
	
	@Autowired
	private PacienteRepository pacienteRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private UnidadeDeAtendimentoRepository unidadeDeAtendimentoRepository;

	Sort sort = Sort.by("dataDoAtendimento").ascending();
	PageRequest paginacao = PageRequest.of(0, 400, sort);

	@GetMapping("listaAtendimentosJSON")
	public List<Paciente> listagemAtendimentosOrdenadosPorDataAscendenteJSON() {
		
		return pacienteRepository.findAll(sort);
	}

	@GetMapping("listaAtendimentosVUEJS")
	public Page<Paciente> listagemAtendimentosOrdenadosPorDataAscendenteVUE() {
		
		return pacienteRepository.findAll(paginacao);
	}

	@GetMapping("paciente/{id}")
	public Optional<Paciente> buscaPacientePorId(@PathVariable(value="id") Integer id) {
		
		return pacienteRepository.findById(id);
	}

	@PostMapping("paciente")
	public Paciente salvaPacienteRest(@Valid @RequestBody RequisicaoNovoAtendimento requisicao) {

		String unidadeDeAtendimentoDigitada = requisicao.getUnidadeDeAtendimento();
		
		UnidadeDeAtendimento unidadeDeAtendimentoCadastrada = unidadeDeAtendimentoRepository.findByNomeUnidadeDeAtendimento(unidadeDeAtendimentoDigitada);
				
		String username = SecurityContextHolder.getContext().getAuthentication().getName();

		User usuario = userRepository.findByUsername(username);
		
		Paciente paciente = requisicao.toPaciente();
		paciente.setUnidadeDeAtendimento(unidadeDeAtendimentoCadastrada);
		paciente.setUser(usuario);

		return pacienteRepository.save(paciente);
	}
	
	@ExceptionHandler(IllegalArgumentException.class)
	public String onError() {
		return "redirect:/rest";
	}
	
	public Boolean verificaExistenciaDaUnidadeDeAtendimento(String unidadeDeAtendimento) {
	
		return true;
	}
}
