package br.com.dbserver.alessandro.desafio_pandemia.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class UnidadeDeAtendimento {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String nomeUnidadeDeAtendimento;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "unidadeDeAtendimento", fetch = FetchType.LAZY)
	private List<Paciente> pacientes;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getNomeUnidadeDeAtendimento() {
		return nomeUnidadeDeAtendimento;
	}
	public void setNomeUnidadeDeAtendimento(String nomeUnidadeDeAtendimento) {
		this.nomeUnidadeDeAtendimento = nomeUnidadeDeAtendimento;
	}
	
}
