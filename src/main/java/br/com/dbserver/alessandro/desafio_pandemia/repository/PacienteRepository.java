package br.com.dbserver.alessandro.desafio_pandemia.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.dbserver.alessandro.desafio_pandemia.model.Paciente;

public interface PacienteRepository extends JpaRepository<Paciente, Integer>{

	@Cacheable("pacientes")
	Page<Paciente> findAll(Pageable sort);
	
	@Query("select p from Paciente p join p.user u where u.username = :username")
	List<Paciente> findAllByUsuario(@Param("username")String username);
	
	Optional<Paciente> findById(Integer id);
}
