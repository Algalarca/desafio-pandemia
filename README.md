# Desafio da Pandemia
Para execução deste sistema existem alguns pré-requisitos que devem ser atendidos:

* O Sistema foi criado no Java 8;
* O sistema roda em um banco MySQL ou MariaDB nas versões mais recentes;
* É necessário um gerenciador de banco de dados SQL - eu recomendo o Heidi SQL (o mesmo que uso, mas pode ser qualquer um);
* É necessário criar um banco de dados com o nome 'desafio_pandemia_dbserver' dentro do gerenciador;
* alguns scripts são necessários para criar as tabelas principais de autenticação (authorities e users), a tabela da Unidade de Atendimento e scripts para popular minimamente o banco;
* Os scripts abaixo devem ser executados **ANTES** do Sistema entrar em funcionamento (na IDE);

##### Script da tabela de usuários:
```sh
CREATE TABLE IF NOT EXISTS `users` (
    `username` varchar(50) NOT NULL,
    `password` varchar(500) NOT NULL,
    `enabled` tinyint(1) NOT NULL,
    PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

##### Script da tabela de autoridades:
```sh
CREATE TABLE IF NOT EXISTS `authorities` (
    `username` varchar(50) NOT NULL,
    `authority` varchar(50) NOT NULL,
    UNIQUE KEY `ix_auth_username` (`username`,`authority`),
    CONSTRAINT `fk_authorities_users` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

##### Script da tabela de unidades de atendimento:
```sh
CREATE TABLE IF NOT EXISTS `unidade_de_atendimento` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `nome_unidade_de_atendimento` varchar(255) DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
```

##### Os scripts abaixo populam o banco:
###### Usuario 'joao', senha 'joao12' (as senhas dos usuários são criptografadas)
```sh
INSERT INTO `users` (`username`, `password`, `enabled`) VALUES ('joao', '$2a$10$Fofd7XC/FGvXswAdNzhVp.phFQ2AA2fBKkw3xk0pmcSs11K4xtRuC', 1);
```
###### Usuária 'maria', senha 'maria12'
```sh
INSERT INTO `users` (`username`, `password`, `enabled`) VALUES ('maria', '$2a$10$sHzhr8SdjDVAtghaClDctudEpiuo3J5bEv6yhjNFN.cFNg6lLdj0O', 1);
```

###### Adiciona os usuários às roles de ADM:
```sh
INSERT INTO `authorities` (`username`, `authority`) VALUES ('joao', 'ROLE_ADM');
INSERT INTO `authorities` (`username`, `authority`) VALUES ('maria', 'ROLE_ADM');
```

###### Popula a tabela de Unidades de Atendimento com 2 unidades:
```sh
INSERT INTO `unidade_de_atendimento` (`id`, `nome_unidade_de_atendimento`) VALUES (1, 'Unidade01');
INSERT INTO `unidade_de_atendimento` (`id`, `nome_unidade_de_atendimento`) VALUES (2, 'Unidade02');
```

O Sistema tem endpoints para funcionar tanto com o padrão MVC tanto quanto o padrão REST (JSON).

#### Alguns pontos para observar:
* Existem vários pontos de melhoria, alguns ajustes e código que pode ser refatorado, principalmente no frontend, mas não consegui implementar por falta de conhecimento;
* Há uma lista de TODOs (não inclusa neste documento, porém na versão anterior a esta enviada) para pontos que precisam de atenção/refatoração/revisão/etc;
* É necessário deixar claro que no padrão MVC, apesar de estar funcionando com o básico, o Sistema não conta com o Frontend totalmente desenvolvido;
* No padrão REST, o Sistema apresenta saída dos dados cadastrados em JSON, frontend para listagem dos atendimentos cadastrados e formulário de novo atendimento feitos com HTML, JavaScript e framework VueJS com Axios;
* Foram utilizadas algumas bibliotecas externas OpenSource para uso junto ao VueJS (incluindo o prório VueJS). A lista completa está no fim deste documento;
* Por eu ter bem poucos conhecimentos em HTML e JavaScript, na parte de cadastro de novo atendimento, no caso de preenchimento de informações erradas, o sistema não cadastra o atendimento, porém também não dá o feedback visual do erro;
* O sistema não mostra no frontend o tempo de duração da consulta, mas quando o cadastro é finalizado o tempo é calculado no backend e adicionado no banco corretamente;
* Quando do cadastro de novo atendimento com retorno HTTP 200, ou seja, cadastro com sucesso, é apresentado um 'alert' para o usuário;
* Este mesmo 'alert', por erro, também é apresentado caso o cadastro tenha sido rejeitado.

### Endpoints
#### (padrão REST):
http://localhost:8080/rest
- pode ser considerado a página inicial do Sistema; há a listagem dos atendimentos cadastrados e um botão para 'Novo Atendimento'

http://localhost:8080/rest/novoAtendimento
 - é o formulário de 'Novo Atendimento'

http://localhost:8080/api/listaAtendimentosJSON
 - uma lista de todos os atendimentos realizados, no formato JSON (pode, por exemplo, ser consumido por um frontend independente)

--
##### Bibliotecas externas utilizadas:
| Biblioteca | Link |
| ------ | ------ |
| VueJS | [https://vuejs.org/] |
| Axios | [https://github.com/axios/axios] |
| Popper | [https://popper.js.org/] |
| Portal-Vue | [https://portal-vue.linusb.org/] |
| Vue-Picker | [https://github.com/invisiburu/vue-picker] |
| Vue-The-Mask | [https://www.npmjs.com/package/vue-the-mask] |
